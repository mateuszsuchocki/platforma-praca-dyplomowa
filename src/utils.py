import pandas as pd

from pydantic import BaseModel


class MLModel(BaseModel):
    class_report: object | None
    test_results: dict | None
    grid: object | None
    conf_matrix: object | None
    train_time: object | None


class Results(BaseModel):
    naive_bayes: MLModel | None
    logistic_regression: MLModel | None
    decision_tree: MLModel | None
    svm: MLModel | None
    random_forest: MLModel | None
    times: dict | None


def file_to_df(data_file, features_file):
    features = pd.read_csv(features_file, names=['name'])
    features_name_list = features['name'].values.tolist()

    df = pd.read_csv(data_file, names=features_name_list, header=None)
    return df
