import streamlit as st


st.title('O projekcie')

st.write("""
Projekt realizuje testy porównawcze algorytmów uczenia maszynowego do klasyfikacji zagrożeń 
w ruchu sieciowym. Rozwiązanie jest zaimplementowane w postaci platformy internetowej przeznaczonej 
dla użytkownika. Według założeń, zbiory danych przyjmowane na 
wejście zawierają przepływy ruchu sieciowego - normalne połączenia sieciowe oraz złośliwe połączenia reprezentowane 
przez konkretne ataki. Aplikacja trenuje 5 modeli przy pomocy różnych algorytmów uczenia maszynowego. 
Wytrenowane modele są poddawane ocenie, a ich podstawowe metryki są prezentowane użytkownikowi w postaci wykresów, 
tabel i diagramów. Sama aplikacja nie analizuje zwracanych wyników, ale pozostawia to zadanie użytkownikowi. 
Dodatkowo aplikacja posiada moduł predykcji pozwalający użytkownikowi wgrać dane nieoznakowane, a następnie wybrać 
wytrenowany wcześniej klasyfikator, który zajmie się predykcją etykiet klas. Wyniki predykcji są prezentowane 
w aplikacji oraz w postaci oznakowanych przez klasyfikator danych zapisanych do pliku.
""")
