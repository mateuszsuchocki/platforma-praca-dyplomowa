import joblib
import pandas as pd
import scipy
import streamlit as st

from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler, OneHotEncoder


def predict_data(base_data_file, predict_data_file, chosen_model):
    if chosen_model == 'Klasyfikator naiwnego bayesa':
        model = joblib.load('pickles/nb_gs.pkl')

    elif chosen_model == 'Regresja logistyczna':
        model = joblib.load('pickles/lr_gs.pkl')

    elif chosen_model == 'Drzewo decyzyjne':
        model = joblib.load('pickles/dt_gs.pkl')

    elif chosen_model == 'SVM':
        model = joblib.load('pickles/svm_gs.pkl')

    elif chosen_model == 'Las losowy':
        model = joblib.load('pickles/rf_gs.pkl')

    else:
        st.error('Nie ma takiego modelu')

    base_data = pd.read_csv(base_data_file, header=None)
    predict_data = pd.read_csv(predict_data_file, header=None)

    X_base_train_data = base_data.drop(base_data.iloc[:, -1:], axis=1)

    obj_columns = X_base_train_data.select_dtypes(include='object').columns
    num_columns = X_base_train_data.select_dtypes(exclude='object').columns
    transforms = ColumnTransformer([
        ("scaler", StandardScaler(), num_columns),
        ("encoder", OneHotEncoder(handle_unknown='infrequent_if_exist'), obj_columns)
    ])
    transforms.fit(X_base_train_data)
    predict_data = transforms.transform(predict_data)

    if type(predict_data) == scipy.sparse._csr.csr_matrix:
        predict_data = predict_data.toarray()

    prediction = pd.DataFrame()
    prediction['type'] = model.predict(predict_data)

    return prediction
