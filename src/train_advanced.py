import warnings
import joblib
import numpy as np
import pandas as pd
import scipy
import seaborn as sns

from matplotlib import pyplot as plt
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import precision_score, recall_score, f1_score, confusion_matrix, accuracy_score, \
    classification_report
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.tree import DecisionTreeClassifier
from datetime import datetime
from utils import Results, MLModel

warnings.filterwarnings("ignore")


def model(model_name, X_train, Y_train, X_test, Y_test):

    start = datetime.now()
    model_name.fit(X_train, Y_train)
    print('Time taken:', datetime.now() - start)

    results_test = dict()
    y_test_pred = model_name.predict(X_test)

    results_test['accuracy'] = accuracy_score(Y_test, y_test_pred)
    print('Accuracy score:')
    print(accuracy_score(Y_test, y_test_pred))
    results_test['precision'] = precision_score(Y_test, y_test_pred, average='weighted')
    print('Precision score:')
    print(precision_score(Y_test, y_test_pred, average='weighted'))
    results_test['recall'] = recall_score(Y_test, y_test_pred, average='weighted')
    print('Recall score:')
    print(recall_score(Y_test, y_test_pred, average='weighted'))
    results_test['f1_score'] = f1_score(Y_test, y_test_pred, average='weighted')
    print('F1-score:')
    print(f1_score(Y_test, y_test_pred, average='weighted'))

    labels = Y_test.unique()
    conf_matrix = confusion_matrix(Y_test, y_test_pred, labels=labels)

    FP = conf_matrix.sum(axis=0) - np.diag(conf_matrix)
    FN = conf_matrix.sum(axis=1) - np.diag(conf_matrix)
    TP = np.diag(conf_matrix)
    TN = conf_matrix.sum() - (FP + FN + TP)

    # recall
    results_test['TPR'] = TP / (TP + FN)
    # specificity
    results_test['TNR'] = TN / (TN + FP)
    # precision
    results_test['PPV'] = TP / (TP + FP)
    # negative predictive value
    results_test['NPV'] = TN / (TN + FN)
    # FPR
    results_test['FPR'] = FP / (FP + TN)
    # FNR
    results_test['FNR'] = FN / (TP + FN)
    # false discovery rate
    results_test['FDR'] = FP / (TP + FP)

    # Overall accuracy
    results_test['ACC'] = (TP + TN) / (TP + FP + FN + TN)

    # Mean FPR
    results_test['mean_FPR'] = np.sum(FP / (FP + TN)) / len(labels)
    print('mean FPR', np.sum(FP / (FP + TN)) / len(labels))

    return results_test


def conf_matrix_and_class_report(model, X, Y_real):
    Y_pred = model.predict(X)
    test_labels = Y_real.unique()

    cm = confusion_matrix(Y_real, Y_pred, labels=test_labels)
    cm_df = pd.DataFrame(cm)
    fig = plt.figure(figsize=(20, 15))
    sns.set(font_scale=1.4)
    sns.heatmap(cm_df, annot=True, annot_kws={"size": 12}, fmt='g', xticklabels=test_labels, yticklabels=test_labels)
    plt.ylabel('Klasa rzeczywista')
    plt.xlabel('Klasa przewidywana')

    # Generate classification report
    class_rep = classification_report(Y_real, Y_pred, output_dict=True)
    class_rep = pd.DataFrame.from_dict(class_rep)
    class_rep = class_rep.T
    class_rep = class_rep.iloc[:, :-1]
    class_rep.columns = ['precyzja', 'czułość', 'wynik F1']

    return fig, cm, class_rep


def train(data_file, features_file):
    results = Results()

    features = pd.read_csv(features_file, names=['name'])
    features_name_list = features['name'].values.tolist()

    data = pd.read_csv(data_file, names=features_name_list, header=None)

    # Start training timer
    benchmark_start = datetime.now()

    # Drop rows with null values
    data = data.dropna(axis=0)

    # Drop duplicates
    data.drop_duplicates(subset=features_name_list, keep='first', inplace=True)

    data.to_pickle('pickles/data.pkl')
    data = pd.read_pickle('pickles/data.pkl')

    # Last feature should be a target feature
    X_train, X_test, Y_train, Y_test = train_test_split(data.iloc[:, :-1], data.iloc[:, -1:],
                                                        stratify=data.iloc[:, -1:], test_size=0.3)

    Y_train = Y_train.squeeze()
    Y_test = Y_test.squeeze()

    # Encoding categorical features and scaling continuous features
    obj_columns = X_train.select_dtypes(include='object').columns
    num_columns = X_train.select_dtypes(exclude='object').columns
    transforms = ColumnTransformer([
        ("scaler", StandardScaler(), num_columns),
        ("encoder", OneHotEncoder(handle_unknown='ignore'), obj_columns)
    ])

    transforms.fit(data.iloc[:, :-1])
    X_train = transforms.transform(X_train)
    X_test = transforms.transform(X_test)

    if type(X_train) == scipy.sparse._csr.csr_matrix:
        X_train = X_train.toarray()

    if type(X_test) == scipy.sparse._csr.csr_matrix:
        X_test = X_test.toarray()

    nb_model = MLModel()
    lr_model = MLModel()
    dt_model = MLModel()
    svm_model = MLModel()
    rf_model = MLModel()

    # Models' hyperparameters
    nb_hyperparameter = {'var_smoothing': [10 ** x for x in range(-3, 3)]}
    lr_hyperparameter = {'alpha': [10 ** x for x in range(-5, 0)], 'penalty': ['l1', 'l2']}
    dt_hyperparameter = {'max_depth': [10, 20, 50, 100, 200], 'min_samples_split': [3, 5, 10, 20]}
    svm_hyperparameter = {'alpha': [10 ** x for x in range(-6, -1)], 'penalty': ['l1', 'l2']}
    rf_hyperparameter = {'max_depth': [100, 200, 350, 500], 'n_estimators': [50, 100, 200, 350],
                         'min_samples_split': [3, 5, 10, 20]}

    # Sklearn ML models
    nb = GaussianNB()
    lr = SGDClassifier(loss='log_loss')
    dt = DecisionTreeClassifier(criterion='gini', splitter='best', class_weight='balanced')
    svm = SGDClassifier(loss='hinge')
    rf = RandomForestClassifier(criterion='gini', class_weight='balanced')

    # Grid searches
    nb_grid = GridSearchCV(nb, param_grid=nb_hyperparameter, cv=10, verbose=1, n_jobs=-1)
    lr_grid = GridSearchCV(lr, param_grid=lr_hyperparameter, cv=10, verbose=1, n_jobs=-1)
    dt_grid = GridSearchCV(dt, param_grid=dt_hyperparameter, cv=10, verbose=1, n_jobs=-1)
    svm_grid = GridSearchCV(svm, param_grid=svm_hyperparameter, cv=10, verbose=1, n_jobs=-1)
    rf_grid = GridSearchCV(rf, param_grid=rf_hyperparameter, cv=10, verbose=1, n_jobs=-1)

    # Training
    start = datetime.now()
    nb_grid_results_test = model(nb_grid, X_train, Y_train, X_test, Y_test)
    nb_training_time = datetime.now() - start

    start = datetime.now()
    lr_grid_results_test = model(lr_grid, X_train, Y_train, X_test, Y_test)
    lr_training_time = datetime.now() - start

    start = datetime.now()
    dt_grid_results_test = model(dt_grid, X_train, Y_train, X_test, Y_test)
    dt_training_time = datetime.now() - start

    start = datetime.now()
    svm_grid_results_test = model(svm_grid, X_train, Y_train, X_test, Y_test)
    svm_training_time = datetime.now() - start

    start = datetime.now()
    rf_grid_results_test = model(rf_grid, X_train, Y_train, X_test, Y_test)
    rf_training_time = datetime.now() - start

    benchmark_training = datetime.now() - benchmark_start

    # Getting training and testing results
    # Naive Bayes
    nb_gs = nb_grid.best_estimator_
    conf_mat_fig, cm, class_rep = conf_matrix_and_class_report(nb_gs, X_test, Y_test)

    nb_model.class_report = class_rep
    nb_model.test_results = nb_grid_results_test
    nb_model.grid = nb_grid
    nb_model.conf_matrix = conf_mat_fig
    nb_model.train_time = nb_training_time

    # Logistic regression
    lr_gs = lr_grid.best_estimator_
    conf_mat_fig, cm, class_rep = conf_matrix_and_class_report(lr_gs, X_test, Y_test)

    lr_model.class_report = class_rep
    lr_model.test_results = lr_grid_results_test
    lr_model.grid = lr_grid
    lr_model.conf_matrix = conf_mat_fig
    lr_model.train_time = lr_training_time

    # Decision tree
    dt_gs = dt_grid.best_estimator_
    conf_mat_fig, cm, class_rep = conf_matrix_and_class_report(dt_gs, X_test, Y_test)

    dt_model.class_report = class_rep
    dt_model.test_results = dt_grid_results_test
    dt_model.grid = dt_grid
    dt_model.conf_matrix = conf_mat_fig
    dt_model.train_time = dt_training_time

    # SVM
    svm_gs = svm_grid.best_estimator_
    conf_mat_fig, cm, class_rep = conf_matrix_and_class_report(svm_gs, X_test, Y_test)

    svm_model.class_report = class_rep
    svm_model.test_results = svm_grid_results_test
    svm_model.grid = svm_grid
    svm_model.conf_matrix = conf_mat_fig
    svm_model.train_time = svm_training_time

    # Random forest
    rf_gs = rf_grid.best_estimator_
    conf_mat_fig, cm, class_rep = conf_matrix_and_class_report(rf_gs, X_test, Y_test)

    rf_model.class_report = class_rep
    rf_model.test_results = rf_grid_results_test
    rf_model.grid = rf_grid
    rf_model.conf_matrix = conf_mat_fig
    rf_model.train_time = rf_training_time

    # Combine results
    results.naive_bayes = nb_model
    results.logistic_regression = lr_model
    results.decision_tree = dt_model
    results.svm = svm_model
    results.random_forest = rf_model

    results.times = {'benchmark_training': benchmark_training}

    # Save trained models for predictions
    joblib.dump(nb_gs, 'pickles/nb_gs.pkl')
    joblib.dump(lr_gs, 'pickles/lr_gs.pkl')
    joblib.dump(dt_gs, 'pickles/dt_gs.pkl')
    joblib.dump(svm_gs, 'pickles/svm_gs.pkl')
    joblib.dump(rf_gs, 'pickles/rf_gs.pkl')

    return results
