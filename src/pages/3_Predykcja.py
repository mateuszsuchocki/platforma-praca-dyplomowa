import altair as alt
import pandas as pd
import streamlit as st

from datetime import date
from prediction import predict_data


def display_charts(prediction):
    chart_data = pd.DataFrame({
        'typ ataku': prediction['type'].value_counts().keys().tolist(),
        'ilość próbek': prediction['type'].value_counts().tolist(),
    })

    chart = alt.Chart(chart_data).mark_bar().encode(
        x=alt.X('typ ataku', sort=None),
        y='ilość próbek',
        tooltip=['typ ataku', 'ilość próbek']
    )

    pred_table = prediction.value_counts().reset_index()
    pred_table.columns = ['typ ataku', 'ilość próbek']
    pred_table['procent całości'] = (pred_table['ilość próbek'] / prediction.shape[0] * 100)

    st.altair_chart(chart, use_container_width=True)
    st.table(pred_table)


@st.cache
def convert_df(unlabeled_data, predictions):
    labeled_data = pd.concat([unlabeled_data, predictions], axis=1).reindex(unlabeled_data.index)
    return labeled_data.to_csv(index=False).encode('utf-8')


st.title('Predykcja')
st.write('#### Podaj poniższe dane wejściowe i kliknij "Prześlij", aby rozpocząć predykcję')

base_data_file = st.file_uploader('Plik z oznakowanymi danymi')
predict_data_file = st.file_uploader('Plik z nieoznakowanymi danymi')

chosen_model = st.selectbox(
    'Model predykcyjny',
    ('Naiwny klasyfikator bayesowski', 'Regresja logistyczna', 'Drzewo decyzyjne', 'SVM', 'Las losowy'))


if base_data_file is not None and predict_data_file is not None and chosen_model is not None:
    if st.button('Prześlij'):
        prediction = predict_data(base_data_file, predict_data_file, chosen_model)
        predict_data_file.seek(0, 0)

        unlabeled_data = pd.read_csv(predict_data_file, header=None)
        predict_data_file.seek(0, 0)

        st.session_state['prediction'] = prediction
        st.session_state['unlabeled_data'] = unlabeled_data

if 'prediction' in st.session_state:
    st.subheader("Rozkład etykiet klas")
    display_charts(st.session_state['prediction'])

    csv_file = convert_df(st.session_state['unlabeled_data'], st.session_state['prediction'])
    today = date.today()

    st.download_button(
        label="Pobierz plik CSV z predykcjami",
        data=csv_file,
        file_name=chosen_model + '_' + today.strftime('%d%m%Y') + '.csv',
        mime='text/csv',
    )
