import streamlit as st

st.title('Jak korzystać?')

st.write("""
    ### Analiza
    
    Platforma przyjmuje na wejście plik z danymi oraz plik z nazwami atrybutów. 
    Po wytrenowaniu odpowiednich modeli, wyświetlane jest porównanie ich podstawowych metryk oceny skuteczności w 
    postaci wykresów i diagramów. 
    
    #### Plik z oznakowanymi danymi
    Plik z danymi musi być zgodny z formatem CSV, a kolumny nie powinny zawierać nazw atrybutów. Dodatkowo ostatnia
    kolumna w pliku powinna być etykietą klasy.
    
    #### Plik z atrybutami
    Nazwy atrybutów w pliku powinny być oddzielone znakiem nowej linii, a ich liczba musi się zgadzać z liczbą kolumn w 
    pliku z danymi.

    #### Trenowanie
    Po wprowadzeniu wymaganych danych wejściowych do aplikacji, przeprowadzany jest proces trenowania. 
    Wykorzystywane algorytmy uczenia maszynowego to:
    - naiwny klasyfikator bayesowski
    - regresja logistyczna
    - drzewo decyzyjne
    - SVM (maszyna wektorów nośnych)
    - las losowy

    #### Wyniki
    Platforma wyświetla podstawowe informacje o wgranym zestawie danych, a po wytrenowaniu modeli zwracane są
    metryki oceny skuteczności każdego z modeli w postaci wykresów, macierze pomyłek oraz szczegółowe dane 
    dotyczące skuteczności predykcji każdej z etykiet klas. Dodatkowo platforma wyświetla czas trwania procesu 
    trenowania oraz poszczególne czasy trenowania każdego z modeli.
    
    ### Predykcja 
    Ten moduł pozwala na klasyfikację danych za pomocą jednego wybranego przez użytkownika klasyfikatora powstałego 
    w wyniku trenowania. 
    
    #### Plik z oznakowanymi danymi
    Ten plik musi być identyczny z tym podanym w analizie. Jest on konieczny do przetworzenia danych do 
    predykcji do tej samej postaci używanej podczas analizy.
    
    #### Plik z nieoznakowanymi danych
    Ten plik powinien mieć taką samą strukturę (takie same cechy) co plik z oznakowanymi danymi, ale bez etykiet 
    klas. Etykiety klas są przewidywane przez wybrany przez użytkownika klasyfikator.
    
    #### Model predykcyjny
    Wytrenowany model podczas analizy będzie wykorzystywany do przewidywania etykiet klas danych nieoznakowanych.
    
    #### Wyniki
    Wyniki predykcji są przedstawione w postaci wykresu i tabeli oraz możliwe jest pobranie pliku z przewidzianymi 
    etykietami klas na dysk.
""")
