import altair as alt
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import streamlit as st
from sklearn.preprocessing import LabelEncoder

from train_advanced import train
from utils import Results, file_to_df


def run_benchmark(data_file, features_file):
    res = train(data_file, features_file)
    return res


def display_charts(results: Results):
    precision_data_chart = pd.DataFrame(
        [results.naive_bayes.test_results['precision'], results.logistic_regression.test_results['precision'],
         results.decision_tree.test_results['precision'], results.svm.test_results['precision'],
         results.random_forest.test_results['precision']],
        index=['Naiwny klasyfikator bayesowski', 'Regresja logistyczna', 'Drzewo decyzyjne', 'SVM', 'Las losowy'],
        columns=['Precyzja']
    )
    st.subheader('Wykres precyzji')
    st.bar_chart(precision_data_chart)

    recall_data_char = pd.DataFrame(
        [results.naive_bayes.test_results['recall'], results.logistic_regression.test_results['recall'],
         results.decision_tree.test_results['recall'], results.svm.test_results['recall'],
         results.random_forest.test_results['recall']],
        index=['Naiwny klasyfikator bayesowski', 'Regresja logistyczna', 'Drzewo decyzyjne', 'SVM', 'Las losowy'],
        columns=['Czułość']
    )
    st.subheader('Wykres czułości')
    st.bar_chart(recall_data_char)

    fpr_data_char = pd.DataFrame(
        [results.naive_bayes.test_results['mean_FPR'], results.logistic_regression.test_results['mean_FPR'],
         results.decision_tree.test_results['mean_FPR'], results.svm.test_results['mean_FPR'],
         results.random_forest.test_results['mean_FPR']],
        index=['Naiwny klasyfikator bayesowski', 'Regresja logistyczna', 'Drzewo decyzyjne', 'SVM', 'Las losowy'],
        columns=['Odsetek fałszywie pozytywnych']
    )
    st.subheader('Wykres odsetek fałszywie pozytywnych')
    st.bar_chart(fpr_data_char)

    f1_data_char = pd.DataFrame(
        [results.naive_bayes.test_results['f1_score'], results.logistic_regression.test_results['f1_score'],
         results.decision_tree.test_results['f1_score'], results.svm.test_results['f1_score'],
         results.random_forest.test_results['f1_score']],
        index=['Naiwny klasyfikator bayesowski', 'Regresja logistyczna', 'Drzewo decyzyjne', 'SVM', 'Las losowy'],
        columns=['Wynik F1']
    )
    st.subheader('Wykres wyniku F1')
    st.bar_chart(f1_data_char)


def display_class_distribution(data):
    class_distribution = data.iloc[:, -1:].value_counts()
    class_distribution.sort_values(ascending=False)
    class_distribution = class_distribution.to_frame().reset_index()
    class_distribution.columns = ['typ ataku', 'ilość próbek']

    chart = alt.Chart(class_distribution).mark_bar().encode(
        x=alt.X('typ ataku', sort=None),
        y='ilość próbek',
        tooltip=['typ ataku', 'ilość próbek']
    )

    class_distribution['procent całości'] = (class_distribution['ilość próbek'] / data.shape[0] * 100)

    st.subheader("Rozkład etykiet klas")
    st.altair_chart(chart, use_container_width=True)
    st.table(class_distribution)


def display_correlation_matrix(data):
    st.subheader('Macierz korelacji cech')

    obj_columns = data.select_dtypes(include='object').columns
    num_columns = data.select_dtypes(exclude='object').columns

    obj_enc = data[obj_columns].apply(LabelEncoder().fit_transform)
    corr_data = pd.concat([data[num_columns], obj_enc], axis=1, join="inner")

    corr_plot = plt.figure(figsize=(30, 25))
    sns.heatmap(corr_data.corr(), linewidths=.2, annot=True, fmt='.1f')
    st.pyplot(corr_plot)


def display_cofusion_matices(res):
    st.header('Macierze pomyłek')
    st.subheader('Naiwny klasyfikator bayesowski')
    st.write(res.naive_bayes.conf_matrix)

    st.subheader('Regresja logistyczna')
    st.write(res.logistic_regression.conf_matrix)

    st.subheader('Drzewo decyzyjne')
    st.write(res.decision_tree.conf_matrix)

    st.subheader('SVM')
    st.write(res.svm.conf_matrix)

    st.subheader('Las losowy')
    st.write(res.random_forest.conf_matrix)


def display_classification_reports(res):
    st.header('Szczegółowe raporty klasyfikacji')
    st.subheader('Naiwny klasyfikator bayesowski')
    st.table(res.naive_bayes.class_report)

    st.subheader('Regresja logistyczna')
    st.table(res.logistic_regression.class_report)

    st.subheader('Drzewo decyzyjne')
    st.table(res.decision_tree.class_report)

    st.subheader('SVM')
    st.table(res.svm.class_report)

    st.subheader('Las losowy')
    st.table(res.random_forest.class_report)


def display_training_meta(res: Results):
    st.header('Ogólne informacje dotyczące procesu trenowania')
    st.write('Czas trwania procesu trenowania: ', res.times['benchmark_training'])

    st.write('Czas trenowania naiwnego klasyfikatora bayesowskiego:', res.naive_bayes.train_time)
    st.write('Czas trenowania modelu regresji logistycznej:', res.logistic_regression.train_time)
    st.write('Czas trenowania modelu drzewa decyzyjnego:', res.decision_tree.train_time)
    st.write('Czas trenowania modelu SVM:', res.svm.train_time)
    st.write('Czas trenowania modelu lasu losowego:', res.random_forest.train_time)

    st.write('Optymalne hiperparametry naiwnego klasyfikatora bayesowskiego: ')
    nb_hiper = pd.DataFrame.from_dict(res.naive_bayes.grid.best_params_, orient='index', columns=['wartość'])
    st.table(nb_hiper)

    st.write('Optymalne hiperparametry modelu regresji logistycznej: ')
    lr_hiper = pd.DataFrame.from_dict(res.logistic_regression.grid.best_params_, orient='index', columns=['wartość'])
    st.table(lr_hiper)

    st.write('Optymalne hiperparametry modelu drzewa decyzyjnego: ')
    dt_hiper = pd.DataFrame.from_dict(res.decision_tree.grid.best_params_, orient='index', columns=['wartość'])
    st.table(dt_hiper)

    st.write('Optymalne hiperparametry modelu SVM: ')
    svm_hiper = pd.DataFrame.from_dict(res.svm.grid.best_params_, orient='index', columns=['wartość'])
    st.table(svm_hiper)

    st.write('Optymalne hiperparametry modelu lasu losowego: ')
    rf_hiper = pd.DataFrame.from_dict(res.random_forest.grid.best_params_, orient='index', columns=['wartość'])
    st.table(rf_hiper)


st.title('Analiza')
st.write('#### Podaj poniższe dane wejściowe i kliknij "Prześlij", aby rozpocząć benchmark')

data_file = st.file_uploader('Plik z oznakowanymi danymi')
features_file = st.file_uploader('Plik z atrybutami')

if data_file is not None and features_file is not None:
    if st.button('Prześlij'):

        data_df = file_to_df(data_file, features_file)
        data_file.seek(0, 0)
        features_file.seek(0, 0)

        display_class_distribution(data_df)

        display_correlation_matrix(data_df)

        with st.spinner('Proszę czekać...'):
            st.write('Rozpoczynam trenowanie modeli')

            results = run_benchmark(data_file, features_file)

            if 'results' not in st.session_state:
                st.session_state['results'] = results

if 'results' in st.session_state:
    display_charts(st.session_state['results'])
    display_cofusion_matices(st.session_state['results'])
    display_classification_reports(st.session_state['results'])
    display_training_meta(st.session_state['results'])
